<?php
/**
 * The Template for displaying all single posts.
 *
 * @package plasterdogcustomizer
 */

get_header(); ?>

<div class="big-background" style="min-height:800px;background-attachment: fixed; background-position: top center; background-repeat: no-repeat;background-size:cover;background-image: url(<?php the_field('home_background_image'); ?>);">

    <div id="page" class="hfeed site">
  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
    <h1><?php the_title(); ?></h1>
<?php if(get_field('post_author_name')) {?>
  <small>written by: <?php the_field('post_author_name'); ?></small>
  <hr/>
<?php } ?><!-- ends the first condition -->
<?php if(!get_field('post_author_name')) {?>
    
<?php }?> <!-- ends the second outer condition -->  
    <?php the_content(); ?>

<?php the_tags( __( 'Tags: ', 'plasterdogcustomizer' ), ' ', '' ); ?>
  </div><!-- .entry-content -->

  <footer class="entry-footer">
    

    <?php edit_post_link( __( 'Edit', 'plasterdogcustomizer' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <div id="secondary" class="widget-area" role="complementary">

     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>


    <?php endif; // end sidebar widget area ?>
  </div><!-- #secondary -->
  <div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php get_footer(); ?>