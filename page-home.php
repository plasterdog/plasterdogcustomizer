<?php
/**
 * Template Name: Landing Page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package plasterdogcustomizer
 */

get_header(); ?>

	

<!-- THREE FEATURED ITEMS ABOVE CONTENT REGION -->
<?php if( get_field('featured_items_position') == 'above-content' ): ?>
			<!--THE THREE FEATURED ITEMS -->
<div class="big-background" style="min-height:800px;background-attachment: fixed; background-position:top center; background-repeat: no-repeat;background-size:cover;background-image: url(<?php the_field('home_background_image'); ?>);"> >
		
				<ul class="three-focus-items">
					<li>
					<div class="inner-focus-item">						
						<div class="feature-icon">
						<a href="<?php the_field('first_feature_link'); ?>"><img src="<?php the_field('first_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('first_feature_link'); ?>"><h3><?php the_field('first_feature_title'); ?> </h3></a>
						</div><!-- ends feature content -->
						<p><?php the_field('first_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('first_feature_link'); ?>">Find out more</a></p>
					</div>
					</li>
					<li>
					<div class="inner-focus-item">							
						<div class="feature-icon" >
						<a href="<?php the_field('second_feature_link'); ?>"><img src="<?php the_field('second_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('second_feature_link'); ?>"><h3><?php the_field('second_feature_title'); ?> </h3></a>
						</div><!-- ends feature content -->
						<p><?php the_field('second_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('second_feature_link'); ?>">Find out more</a></p>
					</div>
					</li>
					<li>		
					<div class="inner-focus-item">						
						<div class="feature-icon">
						<a href="<?php the_field('third_feature_link'); ?>"><img src="<?php the_field('third_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('third_feature_link'); ?>"><h3><?php the_field('third_feature_title'); ?> </h3></a>
						
						</div><!-- ends feature content -->
						<p><?php the_field('third_feature_excerpt'); ?></p>
						<p class="archive-link"><a href="<?php the_field('third_feature_link'); ?>">Find out more</a></p>
					</div>	
					</li>			
				</ul>
<!-- making the region conditional based on entry -->
<?php if ( $post->post_content!=="" ) {		?>	
		<!--THE CONTENT-->
<div id="page" class="hfeed site"  >
			<div id="content" class="site-content" >
			<div id="primary" class="full-content-area">
				<main id="main" class="full-site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>	
				</main><!-- #main -->
			</div><!-- #primary -->
			</div><!-- #content -->
		</div><!-- #page -->

		<div class="clear"></div>

<?php } ?><!-- ending the condition -->
</div>	<!-- ends big background -->
<?php endif; ?><!-- the radio button clause -->


<!-- THREE FEATURED ITEMS BELOW CONTENT REGION -->
<?php if( get_field('featured_items_position') == 'below-content' ): ?>
<div class="big-background" style="min-height:800px;background-attachment: fixed; background-position: center; background-repeat: no-repeat;background-size:cover;background-image: url(<?php the_field('home_background_image'); ?>);">
<!-- making the region conditional based on entry -->
<?php if ( $post->post_content!=="" ) {		?>		
		<!--THE CONTENT-->
		<div id="page" class="hfeed site" style="margin-top:3em;">
			<div id="content" class="site-content" >
			<div id="primary" class="full-content-area" >
				<main id="main" class="full-site-main" role="main">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; ?>	
				</main><!-- #main -->
			</div><!-- #primary -->
			</div><!-- #content -->
			</div><!-- #page -->
			<div class="clear"></div>
			<?php } ?><!-- ending the condition -->
		
		<!--THE THREE ITEMS-->
				<ul class="three-focus-items">
					<li>
					<div class="inner-focus-item">
						
						<div class="feature-icon">
						<a href="<?php the_field('first_feature_link'); ?>"><img src="<?php the_field('first_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('first_feature_link'); ?>"><h3><?php the_field('first_feature_title'); ?> </h3></a>
						</div><!-- ends feature content -->
						<p><?php the_field('first_feature_excerpt'); ?></p>
					</div>
					</li>
					<li>
					<div class="inner-focus-item">	
						<div class="feature-icon" >
						<a href="<?php the_field('second_feature_link'); ?>"><img src="<?php the_field('second_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('second_feature_link'); ?>"><h3><?php the_field('second_feature_title'); ?> </h3></a>
						</div><!-- ends feature content -->
						<p><?php the_field('second_feature_excerpt'); ?></p>
						</div>
					</li>
					<li>		
					<div class="inner-focus-item">
						<div class="feature-icon">
						<a href="<?php the_field('third_feature_link'); ?>"><img src="<?php the_field('third_feature_image'); ?>"/></a>
						</div>
						<div class="feature-content">
						<a href="<?php the_field('third_feature_link'); ?>"><h3><?php the_field('third_feature_title'); ?> </h3></a>
						</div><!-- ends feature content -->
						<p><?php the_field('third_feature_excerpt'); ?></p>
					</div>	
					</li>			
				</ul>




<div class="clear" style="height:2em;"></div>
</div><!-- ENDS BIG BACKGROUND -->
<?php endif; ?>
<?php get_footer(); ?>
